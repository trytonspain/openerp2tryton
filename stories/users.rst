.. title: Users
.. slug: users
.. date: 2013/01/05 11:38:52
.. tags:
.. link:
.. description: Differences about OpenERP and Tryton interface or features

.. sectnum::

In this page, OpenERP users will find a list of differences they can find on
the way the user interface and the application in general works. As in the
rest of this site, we give for granted that the user already knows how OpenERP
works so we just concentrate on *what changes*.

Clients
=======

Traditionally OpenERP has had several ways to access the system:

- Official desktop GTK client whose support has been dropped as of v7. Some
  voices have pointed to the possibility to let the community support it but
  that will most probably not happen because basically the framework has lost
  the client independence it is required to do that.
- Web client which has been reimplemented several times but has been available
  since version 5. It is now been improved a lot and is the only official client
  since version 7.
- Non-official Qt-based desktop client named Koo_. This client was well
  supported until version 5. It still works on v6 but it does not support
  grouping and search views. This client was developed by `NaN·tic`_.
- Non-official `terminal client`_. Probably never has been feature complete nor
  stable enough for real usage.

As mentioned above, since version 7, given the dependence on HTML in OpenERP
core it will be much more complicated to build generic clients that are not web
based. There are some android clients but are of very limited functionality
(such us only the possibility of reading the list of partners).

.. _Koo: https://launchpad.net/openobject-client-kde
.. _`NaN·tic`: http://www.NaN-tic.com
.. _`terminal client`: http://code.google.com/p/terp-client/

On the other hand Tryton has only had one way to access the system, but now that
is changing:

- Official desktop GTK client.
- Official `jquery-based web client`_ is in the works.
  At `TUL 2012`_, B2CK raised some money to make a first version. From there on,
  both GTK and Web clients will be maintained. There was another attempt to build
  a native web client several years ago but was never finished.
- Non-official `Android client`_ is in the works but it is already available in
  Google Play and in pretty good shape. This client is developed by SCIL_ and
  financed by `Bio Eco Forests`_. The client supports reading and writing to any
  model, has support for tree views, forms and charts. It can even queue write
  operations if there is no Internet connection when changing information.
  Supports Tryton 2.6.
- Nereid_, developed by OpenLabs_, is not a complete client in itself yet it is
  a web framework for developing web sites completely integrated to Tryton. This
  is the best solution for cases for developing online shops, or other services
  to customers or suppliers, much better than creating a user with restricted
  access to the ERP. This is a better solution because the interface can be
  integrated with existing web site and because the ERP interface is not
  intended for third party access but for doing work.

  Nereid uses flask_ and will soon be part of Tryton core.

  Here you can see several `screenshots of Nereid`_ for a project management web
  application, in which all information is stored in Tryton in real time:

.. slides::

   /images/nereid-01.png
   /images/nereid-02.png
   /images/nereid-03.png

.. _`jquery-based web client`: http://hg.tryton.org/sandbox/sao/
.. _`TUL 2012`: http://code.google.com/p/tryton/wiki/Liege2012
.. _`Android client`: http://trac.scil.coop/tryton_android/wiki
.. _SCIL: http://www.scil.coop/
.. _`Bio Eco Forests`: https://projects.bioecoforests.com/
.. _Nereid: http://nereid.openlabs.co.in
.. _OpenLabs: http://openlabs.co.in
.. _flask: http://flask.pocoo.org
.. _`screenshots of Nereid`: http://engineering.openlabs.co.in/post/28108986137/some-screenshots-of-the-upcoming-nereid-interface

Usability
==========

Menus
-----

Tryton uses the same menu structure OpenERP used in the 5.0 days. In our
experience it is easier for understand specially for users that are in charge
of several areas (which is what happens most of the time). Since 6.0, OpenERP
tries to define ERP areas but usually those do not match the processes of a
particular user in a company. What happens then is that the user has several
menu entries to access the same information, which is misleading. For example,
a user may access products from within sales, purchases or warehouse menus.

Tryton also makes the menu always visible and keeps its state avoiding having
to constantly re-open nodes of the tree.

Pre-validation
--------------

Tryton has the concept of record pre-validation. This is useful when introducing
a large number of lines in a sale or invoice, for example because after the user
pushes OK, the line is checked for correctness. This way, if some invalid
information was introduced, the user will know without having to wait until
he wants to save the whole document, when it would be more annoying to fix line
by line.

Decimals
--------

Tryton allows managing decimal places individually by field and record. For
example, decimal places may depend on the currency of the invoice or the unit of
measure of the stock move.

In OpenERP, the developer must decide how many digits will be used for
quantities in sale lines (typically 2) and those will remain always the same. No
matter that the unit of measure is Units which cannot be fractioned.

Instead, in Tryton, changing the unit of measure of a sale line will also change
the number of decimal places in the quantity field. And this is just an example,
this will be consistent all over the application.


Multi-company
=============

Tryton allows the same user to be in different companies simultaneously
because changing the current company will not change the information in the
database but only in the current context.

This means you can have two client instances opened each operating in a
different company of the ERP.


Party
=====

The Party concept in Tryton is more generic and flexible than Partner in
OpenERP because Party includes also employees. The same way OpenERP uses the
same concept for both customers and suppliers, Tryton also includes in it
employees.  This is the right approach because you do not end up with duplicate
information.


Sales
=====

Returns
-------

Tryton allows the user to introduce a negative quantity in sale lines. In that
case, a Customer Return Shipment will be created instead of Customer Shipment.
This is specially useful because it makes it crystal clear what is the price
that should be used for the credit note.

Accounting
==========

- Tryton manages taxes with a one2many from move lines instead a many2one. This
  avoids stupid move lines with debit == credit == 0.0 when a move line has more
  than one tax.

- Dates and periods in lines and moves are consistent. They are normalized and
  avoids things hard to justify to users such as lines with dates different from
  the move (this happens in OpenERP).

- The date of the move it is guaranteed to be in the period of the move. We had
  to develop a module in OpenERP for that because otherwise it makes things
  inconsistent.

- Partial reconciliation in OpenERP is broken: it will show you the wrong
  pending amount in invoices.


Warehouse
=========

Periods & performance
---------------------

Tryton has a couple of features for improved performance:

- Periods: users can periodically close a stock period. Once closed, Tryton
  computes and stores the stock levels of all products, which allows it to reuse
  them whenever the user queries the available stock of a given product.

  This is a basic feature for any warehouse management application if we don't
  performance to degrade rapidly when time passes by, but it is not available
  in OpenERP (in fact, we had to implement that for v5).

- Store quantity in standard unit: when a stock move is created or updated
  Tryton will store its quantity in the product's standard unit of measure. This
  makes the computation of existing stock for a given product really quick.

Order points
------------

Tryton has two types of order points: purchase and internal. The former, has
a similar behaviour to OpenERP ones: it will create a purchase request (in OpenERP
it directly creates a purchase). The latter allows to indicate a provisioning
location and it will create internal shipments to move products from the
provisioning location to the location of the order point.

This is very useful for companies with high rotation locations and storage
locations. It is possible to define order point rules for high rotation
locations so that they are re-filled from storage locations when their stock
decreases.

Note that it is also possible to define order points where the origin is not a
purchase or location but the system will create a production.

Cost price
----------

Unlike OpenERP, Tryton manages the cost prices in stock moves in the right way.
That is, all incoming and outgoing stock moves stores the cost price of the
product, which by default is taken from the product form. Having that cost in
both incoming and outgoing move, makes it possible to calculate the margin in
sales easy and correct.

This is not to be confused with the unit price, which is only required for
supplier or production incoming moves or customer outgoing moves.

Forecast Quantity
-----------------

Virtual Stock in OpenERP is named Forecast Quantity in Tryton, the most
important difference being that Tryton will not take into account moves that do
not have a planned date for computing this value and that moves in *draft* state
are taken into account.

Production
==========

Here's a list of differences between Tryton *production* module and OpenERP
*MRP* one:

- Tryton allows a Bill of Material to have several products in the output by
  default. OpenERP uses an extra module for that.
- Tryton does not define work center, routes or anything that is not a bill of
  materials and a production itself. OpenERP defines some of those concepts,
  although those are used for definition purposes only. There's no logic
  involved with them.
- Tryton allows to have a production without a Bill of Materials, very handy
  in some industries.
- Tryton does not force the usage of a picking previous to the production. So
  it is simpler. If desired, that behaviour can be emulated using internal order
  points.
- With proper configuration of internal order points, it is also possible to
  define the complete workflow of external productions (executed by a supplier).
  And manage in Tryton the full process: shipment of some products to the
  supplier, production and shipment of the final product to the company's
  warehouse.

Supply/Procurement/Planning
===========================

Procurements are very complex in OpenERP. Maybe not specially complex for users
it is for developers due to the workflow associated with them. In Tryton there
is no such a concept of centralized procurement but it has several modules for
managing supplies.

For purchases, there are purchase requests. The main differences with OpenERP
are:

- The purchase is not created automatically even if all the necessary
  information information (such as supplier and price) is available. Only the
  request is created with the suggestion of the supplier to use.
- If order points are evaluated again, the system will remove unprocessed
  requests and thus possibly increasing the required quantities to purchase. In
  OpenERP you'd need to manually remove the purchase in order for the system to
  recalculate correctly.

The *sale_supply* module allows you to create purchase requests directly from
sales. It would be equivalent to OpenERP's supply *On Order*.

Tryton is specially good at managing drop shipments with the
*sale_supply_drop_shipment* module. A very nice feature of this module is that
it creates a new kind of shipment specially designed for goods being delivered
directly by the supplier to the customer. And thus source location for the moves
is the one of the supplier whereas the destination is the one of the customer.

Productions are a bit different than purchases. There is no such production
request but the first state of a production is precisely *request*. Meaning that
if order points are re-executed, all productions in *request* state will be
removed and recalculated.

Projects
========

The main difference in project management in Tryton is that projects and tasks
are are not linked to analytic accounts and thus hours introduced in the
timesheet have no effects in analytics. The main reason for that is that
analytics should be linked to accounting, otherwise it is hard to be sure that
analytics is correct.

It is not available but the idea is to eventually create a wizard that allows
creating the analytic moves based on timesheet when the payroll is introduced
in the accounting typically at the end of the month.

In Tryton there are several core modules related to projects that make this area
more complete than core OpenERP . These include:

- Project Revenue: which lets users know project's cost and revenue.
- Project Plan: Employees and their dedication in terms of percent of
  their time, can be allocated which allows forward and backward planning to
  detect deadlines that cannot be met.
- Project Invoice: which allows users to invoice projects from timesheet lines
  (hours spent) or from planned hours. Developers can also extend this module
  to create other invoicing methods.

Security
========

Tryton prompts the user password after a certain amount of inactivity time. This
is sometimes a requirement in some industries (such as pharmaceuticals) where
the law imposes this sort of behaviour.

