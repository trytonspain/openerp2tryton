.. title: OpenERP to Tryton
.. slug: index
.. date: 2013/01/05 11:38:52
.. tags: 
.. link: 
.. description: Information about move from OpenERP to Tryton


This site aims to collect all the information needed to successfully move from
OpenERP_ to Tryton_. This site explains some of the differences between both 
projects_ and targets both users_ and developers_. It also contains information
on how to migrate_ data from one system to the other.


.. _OpenERP: http://www.openerp.com

.. _Tryton: http://www.tryton.org

.. _projects: /projects.html

.. _users: /users.html

.. _developers: /developers.html

.. _migrate: /migration.html

::

  [5%] Starting Tryton Foundation...
  [10%] Removing vendor lock-ins...
  [15%] Removing double licensing...
  [20%] Including migrations...
  [25%] Sending project feedback...
  [50%] Installing desktop client...
  [60%] Installing web client (Work in Progress)...
  [70%] Installing Android client (Work in Progress)...
  [90%] Installing web integration framework (nereid)...
  [95%] Migrating data from OpenERP to Tryton...
  [100%] System ready!


Why?
====

Tryton was born as a fork of OpenERP in 2009 and both use the same programming 
language (python_), the same main DBMS (PostgreSQL_) and architecture 
principals.

.. _python: http://www.python.org

.. _PostgreSQL: http://www.postgresql.org

These aspects makes it very probable that users and developers working with 
OpenERP which has a lot of momentum, will be interested in knowing more about
Tryton, specially when they realize some of its shortcomings or limitations.

This is not to say that Tryton is perfect or even always a better option than
OpenERP, but it is slowly yet steadily getting there.

One might think why we did not create a Navision2Tryton or Openbravo2Tryton but 
the main reason is that those are completely different systems while OpenERP and 
Tryton are not all that different in the end. Also, we have a good in-depth 
knowledge of both OpenERP and Tryton so we think we can offer useful 
information, which would not be the case with Navision or Openbravo.

Even more, in our process to change our customers from one system to the other, 
we're going to collect a large amount of information and scripts that will 
certainly be useful for our own internal usage, and most probably will be for 
others too.

More information
================

You may also find this comparison useful: http://en.wikipedia.org/wiki/Comparison_of_Tryton_and_Open_ERP

If you find differences between OpenERP and Tryton and want to share them,
please, contact us and we will publish them in this site. You may also fork this
project and make a merge proposal at http://www.bitbucket.org/trytonspain/openerp2tryton
