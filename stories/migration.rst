.. title: Migration
.. slug: migration
.. date: 2013/01/05 11:38:52
.. tags: 
.. link: 
.. description: KafkaDB. Migration OpenERP database to Tryton 

At `NaN·tic`_ we worked hard to deliver a migration technology that made the 
tricky migration process from OpenERP to Tryton as simple and reusable as 
possible.  After a good amount of research and development we created KafkaDB_, 
which is the basis of the migration process. Later, with the help of 
Zikzakmedia_, we created a large number of `KafkaDB Jobs`_, that allow anyone 
with the appropriate knowledge to migrate their existing OpenERP database to 
Tryton. 

This includes **accounting**, **invoices**, **customers**, **suppliers**, 
**products**, **sales**, **stocks** and much more information.

Both, KafkaDB and Jobs are open source and freely available. And of course,
we encourage everyone to join and contribute to the projects.

.. _`NaN·tic`: http://www.NaN-tic.com

.. _KafkaDB: https://bitbucket.org/angelnan/kafkadb/wiki/Home

.. _Zikzakmedia: http://www.zikzakmedia.com

.. _`KafkaDB Jobs`: https://bitbucket.org/angelnan/kafkadb_openerpv5_tryton/src

Those who can read Spanish, may find these slides useful to understand
KafkaDB's architecture:

.. raw:: html

   <center>
   <iframe 
        src="http://www.slideshare.net/slideshow/embed_code/13190976" 
        width="597" 
        height="486"
        frameborder="0" 
        marginwidth="0" 
        marginheight="0" 
        scrolling="no"
        style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" 
        allowfullscreen 
        webkitallowfullscreen 
        mozallowfullscreen> 
        </iframe> 
        <div style="margin-bottom:5px"> 
        <strong>
            <a 
                href="http://www.slideshare.net/NaN-tic/kafkadb-13190976" 
                title="Kafkadb" 
                target="_blank">Kafkadb</a> 
                </strong> from <strong>
                <a href="http://www.slideshare.net/NaN-tic" target="_blank">NaN·tic</a>
        </strong>
        </div>
    </center>
