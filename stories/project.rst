.. title: Projects
.. slug: projects
.. date: 2013/01/05 11:38:52
.. tags:
.. link:
.. description: Differences on project organization between OpenERP and Tryton

.. sectnum::

This page documents some differences on project organization between OpenERP and
Tryton. In general we give for granted that you know more or less how OpenERP
is organized.

Foundation
==========

On November, 2012 the `Tryton Foundation`_ was created in Belgium and with it its
first Board of Directors. The Foundation is in some way in charge to  replace
the figure of OpenERP SA in the case of OpenERP. It is there to:

.. _`Tryton Foundation`: http://foundation.tryton.org

- develop and support conferences, meetings and community activities
- hold and administer tryton.org infrastructure
- organize the community of supporters
- manage and promote Tryton

It also shows the world and specially end users and companies that the community
is well organized. Companies tend to search for something solid and probably the 
Foundation is the way to make this clear while keeping the vendor independence 
they also desire.


Data migration between versions
===============================

Tryton includes data migration when a new version is installed. This is a clear
advantage over OpenERP because the migration effort is shared between all
participants. In OpenERP we can easily share modules, but migrations are not so
easy to share and thus either users are locked to the editor or they have to
spend a larger amount of money than they would if everything was included as in
Tryton.


Time-based releases
===================

Tryton releases a new major version every 6 months and it is very regular. The 
reason is that the development version is kept very stable during the whole 
development process and new features are only added when they are ready, and in 
the last month no new features are added but is reserved for bug fixing.

Here's the list of all Tryton releases starting at 1.0 with its planned release
dates and real release dates:

=======  ============  =========  =======
Version  Planned Date  Real Date  Delay
=======  ============  =========  =======
1.0          17/11/08   17/11/08  0 days
1.2          20/04/09   20/04/09  0 days
1.4          19/10/09   20/10/09  1 day
1.6          03/05/10   17/05/10  14 days
1.8          25/10/10   07/11/10  13 days
2.0          20/04/11   29/04/11  9 days
2.2          24/10/11   26/10/11  2 days
2.4          23/04/12   25/04/12  2 days
2.6          22/10/12   23/10/12  1 day
2.8          22/04/13   22/04/13  0 days
=======  ============  =========  =======

This predictability is a huge advantage for users and integrators because it
allows proper planning of deployment of new versions.

In contrast, OpenERP follows a feature-based release cycle and usually releases
too soon, because new features and big patches are applied only days before 
final release. This results in very unstable *dot zero* versions which tends to 
make experienced integrators wait several months before relying on them.


License
=======

OpenERP and Tryton have similar but different licenses. OpenERP uses AGPLv3 
while Tryton uses GPLv3.

The main difference is that if you make a development in OpenERP which can be
considered a derivative work (a change on the client or a module will most 
probably be), and you give access to those changes to third parties such as 
customers or suppliers (or you provide some kind of SaaS) you must make the 
source code available to them.

The same is not true in Tryton and you're only forced to provide the source code
if you distribute the application in binary form.

Both have advantages and drawbacks: 

- The former does not allow *private* (not publishing the code) SaaS except if 
  that is provided by OpenERP SA who owns the code. This creates incentives to
  integrators to free their code and means there's a large number of modules
  available. 
  
  At the same time OpenERP SA has a double license model and you can acquire a 
  proprietary version which will allow you to provide this SaaS, but of course, 
  you will not be able to use most community modules unless the authors sell you 
  proprietary licenses.

- The latter will allow you to create your own modules without publishing the 
  code to your customers and suppliers (which may also be your competitors).
  
  At the same time it does not create incentives to publish the code although it
  gives more freedom to Tryton users.


Security
========

Tryton takes security quite seriously: opens CVEs and makes special `security
releases`_ if security issues are found.

In contrast, take for example `this bug`_ reported to OpenERP where the bug is 
marked as incomplete and several of the issues are still not fixed today more 
than two years later.

.. _`this bug`: https://bugs.launchpad.net/openobject-server/+bug/657013
.. _`security releases`: http://news.tryton.org/2012/09/security-releases-for-trytond-24-series.html

